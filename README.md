# Elmo's KiCad library

This is a library for my custom **footprints**, **symbols**, and **models** for my **KiCad** projects. Feel free to use them. At your own risk.

## How do I use these?

*TBA*

## Contributors

Elmo Rohula
